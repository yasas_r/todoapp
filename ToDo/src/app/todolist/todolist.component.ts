import { Component, OnInit} from '@angular/core';
import { Todo } from '../todo';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {

  public toDoList: Todo[];

  public errorMessage = "";

  public todoInputNg:string="";

  constructor(private _toDoService: TodoService) {
    
  }

  ngOnInit() {
    this.toDoList = this._toDoService.getToDoList();
  }

  addItem(todoInput:string){
    
    if (this.validateInput(todoInput)) {  
      this.toDoList = this._toDoService.addTodo(todoInput);
      this.todoInputNg = null;
      this.errorMessage = "";
    }
    else{
      this.errorMessage = "Please enter ToDO item";
    }

  }

  remove(id:number){
    this.toDoList = this._toDoService.removeTodo(id);
  }

  complete(todo:Todo){

    if(todo.complete==false){
      this._toDoService.completeTodo(todo.id);   
    }
    else{
      this._toDoService.inCompleteTodo(todo.id);
    } 

  }

  getColor(todo:Todo):string {

    if(todo.complete==true){
      return "complete";
    }
    else{
      return "not-complete"
    }
  }

  validateInput(todoInput:string){

    if(todoInput){
      return true;
    }
    else{
      return false;
    }
    
  }

}
