import { Injectable } from '@angular/core';
import { Todo } from './todo';

@Injectable()
export class TodoService {

  private toDoList : Todo[]=[
     
     {id: 0,title: 'Hello 1', complete: false}
  ];

  lastId: number = 0;

  constructor() { }

  getToDoList():Todo[]{
    return this.toDoList;
  }

  addTodo(title:string): Todo[] {

    let todo = new Todo(title);
    todo.id = ++this.lastId;
    this.toDoList.push(todo);

    return this.toDoList;
    
  }

  removeTodo(id: number): Todo[]{

    this.toDoList = this.toDoList.filter(todo => todo.id !== id);
    return this.toDoList;
    
  }

  completeTodo(id: number) {
    let todo = this.getTodoById(id);
    todo.complete = true;  
  }

  inCompleteTodo(id: number) {
    let todo = this.getTodoById(id);
    todo.complete = false;   
  }

  getTodoById(id: number): Todo {
    return this.toDoList.filter(todo => todo.id === id).pop();
  }

  isComplete(id: number):boolean{
    let todo = this.getTodoById(id);
    return todo.complete?true:false;
  }

  
}
