export class Todo {

    id: number;
    title: string = '';
    complete: boolean = false;


    constructor(title:string) {
        this.title = title;
    }


}
